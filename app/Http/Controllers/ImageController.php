<?php

namespace App\Http\Controllers;

use App\Image;
use App\Like;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;

class ImageController extends BaseController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['file' => 'required | mimes:jpeg,jpg,png|max:5000']);

        $imageManager = new ImageManager();

        $ext = $request->file('file')->extension();
        $temp = sys_get_temp_dir() . '/' . uniqid() . ".$ext";
        $imageManager
            ->make($request->file('file'))
            ->resize(600, 600)
            ->save($temp);
        Storage::disk('s3')->put('public/' . basename($temp), file_get_contents($temp), 'public');
        $url = Storage::disk('s3')->url('public/' . basename($temp));

        $image = new Image();
        $image->title = $request->input('title');
        $image->s3 = $url;
        $image->save();

        return response()->json(
            'ok'
        );
    }


    public function toggleLike($imageId)
    {

        $image = Image::with([
            'likes' => function ($q) {
                $q->where('user_id', Auth::id());
            }
        ])->find($imageId);

        if (!$image->likes->count()) {
            $like = new Like();
            $like->user_id = Auth::id();
            $like->image_id = $imageId;
            $like->active = true;
        } else {
            $like = $image->likes->first();
            $like->active = !$like->active;
        }
        $like->save();

        $image = Image::with([
            'likes' => function ($q) {
                $q->where('active', true);
            }
        ])->find($imageId);

        $image->likes_count = $image->likes->count();
        unset($image->likes);

        return response()->json(
            $image
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::with([
            'likes' => function ($q) {
                $q->where('active', true);
            }
        ])->get()->keyBy('id');

        foreach ($images as $image) {
            $image->likes_count = $image->likes->count();
            unset($image->likes);
        }

        return response()->json(
            $images
        );
    }
}
