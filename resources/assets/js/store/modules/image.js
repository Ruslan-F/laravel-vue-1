import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  images: [],
}

// getters
export const getters = {
  list: state => state.images
}

// mutations
export const mutations = {
  [types.SET_IMAGES] (state, images) {
    state.images = images
  },
  [types.UPDATE_IMAGE] (state, image) {
    state.images[image.id] = image
  },
}

// actions
export const actions = {
  async fetchImages ({commit}) {
    try {
      const {data} = await axios.get('/api/image');
      commit(types.SET_IMAGES, data)
    } catch (e) {
      console.error(e)
    }
  },

  async fetchImage ({commit}, imageId) {
    try {
      const {data} = await axios.get(`/api/image/${imageId}`);
      commit(types.UPDATE_IMAGE, data)
    } catch (e) {
      console.error(e)
    }
  }
}