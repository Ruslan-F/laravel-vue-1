# ABOUT.md
## Tech used:
* Project was developed on Homestead env (requires Node v. !> 8.11.3).
* Based on this [**laravel-vue-spa**](https://github.com/cretueusebiu/laravel-vue-spa) boilerplate to speed auth / scaffolding part.
* Used composer require league/flysystem-aws-s3-v2 for S3 store handling;
* Used composer require intervention/image for uploaded image sizes handling;
* Using default **laravel-vue-spa** auth logic;
* Made as SPA, using router / vuex store;
* Comes with l18n out of the box (English / Chinese / Spanish languages), added some English strings to ALL locale files, just for demonstration.
* Not much markup / fancy design here, but there's also not much time for it really ;).

## Configuration:
* Those variables inside `.env` file should be filled with real one, so s3 could work:


    AWS_KEY=
    AWS_SECRET=
    AWS_REGION=
    AWS_BUCKET=
    

* Requires all default deployment pipelines, e.g.
    
    
    composer install
    yarn install
    php artisan migrate
    npm run watch


## Implemented features:
* All features listed on the Upwork;
* Plus: Project is SPA
* Plus: Little preview element for image on upload page;
* Plus: Bit of basic validation on front / backend for file type / title;
* Plus: Images being resized to 600 / 600 after upload on backend;
* When toggling image Like - only toggled image being updated to / from backend;  
